from exponent_server_sdk import DeviceNotRegisteredError
from exponent_server_sdk import PushClient
from exponent_server_sdk import PushMessage
from exponent_server_sdk import PushResponseError
from exponent_server_sdk import PushServerError
from requests.exceptions import ConnectionError
from requests.exceptions import HTTPError
import threading
import datetime
import sqlite3
import json
import background


def send_push_message(token, message, info_amount, extra, conn, c):
    try:
        response = PushClient().publish(PushMessage(
            to=token,
            body=message,
            badge=info_amount,
            data=extra,
            priority='high'
        ))
    except PushServerError as exc:
        print('PushServerError:\n' + json.dumps({
            'token': token,
            'message': message,
            'info_amount': info_amount,
            'extra': extra,
            'errors': exc.errors,
            'response_data': exc.response_data
        }))
        raise
    except (ConnectionError, HTTPError) as exc:
        print('ConnectionError:\n' + json.dumps({
            'token': token,
            'message': message,
            'info_amount': info_amount,
            'extra': extra,
            'exc': exc
        }))
        raise

    try:
        response.validate_response()
    except DeviceNotRegisteredError:
        try:
            c.execute('DELETE FROM users WHERE token=?;', (token,))
            conn.commit()
            print(str(int(c.execute('SELECT count(*) FROM users;').fetchone()[
                              0]) + 1) + 'th user was removed due to uninstalling the app.')
        except sqlite3.OperationalError:
            print('OperationalError:\n' + json.dumps({
                'token': token,
                'message': message,
                'info_amount': info_amount,
                'extra': extra
            }))
            raise
    except PushResponseError as exc:
        print('ConnectionError:\n' + json.dumps({
            'token': token,
            'message': message,
            'info_amount': info_amount,
            'extra': extra,
            'push_response': exc.push_response
        }))
        raise


class Notifications:
    def __init__(self, database_name, background_request):
        self.database_name = database_name
        self.background_request = background_request

        conn = sqlite3.connect(database_name)
        c = conn.cursor()
        c.execute('CREATE TABLE IF NOT EXISTS users (token TEXT, grade TEXT, notification BOOLEAN, daytime TEXT)')
        conn.commit()
        conn.close()

        now = datetime.datetime.now()
        threading.Timer(60.0 - (now.second + (now.microsecond / 1000000)), self.notify_users).start()

    def notify_users(self):
        threading.Timer(60.0 - (datetime.datetime.now().microsecond / 1000000), self.notify_users).start()

        now = datetime.datetime.now()
        now_daytime = now.strftime("%H%M")
        plan_date = now.date() + datetime.timedelta(days=1 if now.hour > 16 else 0)

        conn = sqlite3.connect(self.database_name)
        c = conn.cursor()
        for row in c.execute('SELECT * FROM users WHERE notification="1" AND daytime=?', (now_daytime,)):
            plan_crawlers = self.background_request.get_plan_crawlers()
            for plan_crawler in plan_crawlers:
                crawler, infos = plan_crawler['crawler'], plan_crawler['infos']

                date_s = crawler.find('div', class_='mon_title').text.strip()
                date_formatted = background.format_date(date_s)

                date = datetime.datetime.strptime(date_s.split(' ')[0], "%d.%m.%Y").date()
                if date != plan_date:
                    continue
                class_parts = row[1].split('/')
                grade = class_parts[0]
                courses = class_parts[1] if len(class_parts) > 1 else ''

                info_amount = len(infos)

                lessons = background.get_lessons(crawler, grade, courses)
                lesson_amount = len(lessons)

                weekdays = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So']
                text = 'Am {}., {}.{}. gibt es '.format(weekdays[date.weekday()], str(date.day).zfill(2),
                                                        str(date.month).zfill(2))
                if info_amount == 0 and lesson_amount == 0:
                    text += 'keine Informationen oder Vertretungsstunden'
                elif info_amount != 0 and lesson_amount == 0:
                    text += '{} Information{}'.format(info_amount, 'en' if info_amount > 1 else '')
                elif info_amount == 0 and lesson_amount != 0:
                    text += '{} Vertretungsstunde{}'.format(lesson_amount, 'n' if lesson_amount > 1 else '')
                else:  # info_amount != 0 and lesson_amount != 0
                    text += '{} Information{} und {} Vertretungsstunde{}'.format(
                        info_amount,
                        'en' if info_amount > 1 else '',
                        lesson_amount,
                        'n' if lesson_amount > 1 else ''
                    )
                text += ' für Dich.'

                send_push_message(row[0], text, info_amount + lesson_amount,
                                  {'date': date_formatted, 'infos': infos, 'lessons': lessons}, conn, c)
                break
        conn.close()
