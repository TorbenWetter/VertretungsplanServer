# -*- coding: utf-8 -*-
from flask import Flask, request
from waitress import serve
import hashlib
from Crypto.Cipher import XOR
import base64
import json
import re
import sqlite3
import background
import notifications

database_name = 'users.db'

app = Flask(__name__)

pw_file = open('password.txt', 'r')
pw = pw_file.readline().strip()
pw_sha1 = hashlib.sha1(pw.encode('utf-8')).hexdigest()
pw_file.close()

background_request = background.Background(pw_sha1)
notifications.Notifications(database_name, background_request)

key_file = open('key.txt', 'r')
xor_key = key_file.readline().strip()
key_file.close()

my_token_file = open('my_token.txt', 'r')
my_token = my_token_file.readline().strip()
my_token_file.close()

with open('classes.json', 'r') as classes_file:
    classes = classes_file.read()


def decrypt_pw(encrypted_pw):
    cipher = XOR.new(xor_key)
    return cipher.decrypt(base64.b64decode(encrypted_pw)).decode('utf-8')


@app.route('/', methods=['POST'])
def valid():
    data = request.get_json()
    password = data['password'] if 'password' in data else ''
    encrypted = bool(data['encrypted']) if 'encrypted' in data else False

    pw_valid = pw == (decrypt_pw(password) if encrypted else password)
    answer = {
        'valid': pw_valid
    }

    if pw_valid and not encrypted:
        answer['key'] = xor_key

    return json.dumps(answer, ensure_ascii=False)


@app.route('/classes', methods=['POST'])
def get_classes():
    data = request.get_json()
    password = decrypt_pw(data['password']) if 'password' in data else ''

    pw_valid = pw == password
    answer = {
        'valid': pw_valid
    }

    if pw_valid:
        answer['data'] = classes

    return json.dumps(answer, ensure_ascii=False)


@app.route('/plans/<grade>', methods=['POST'])
@app.route('/plans/<grade>/<courses>', methods=['POST'])
def get_plans(grade, courses=''):
    data = request.get_json()
    password = decrypt_pw(data['password']) if 'password' in data else ''

    pw_valid = pw == password
    answer = {
        'valid': pw_valid
    }
    if not pw_valid:
        return json.dumps(answer, ensure_ascii=False)

    answer['plans'] = []
    plan_crawlers = background_request.get_plan_crawlers()
    for plan_crawler in plan_crawlers:
        crawler, infos = plan_crawler['crawler'], plan_crawler['infos']

        date_formatted = background.format_date(crawler.find('div', class_='mon_title').text.strip())

        answer['plans'].append({
            'date': date_formatted,
            'data': {
                'infos': infos,
                'lessons': background.get_lessons(crawler, grade, courses)
            }
        })

    return json.dumps(answer, ensure_ascii=False)


@app.route('/grade', methods=['POST'])
def set_grade():
    data = request.get_json()
    password = decrypt_pw(data['password']) if 'password' in data else ''
    token = data['token']
    grade = data['grade']

    pw_valid = pw == password
    if not pw_valid or not token or not grade:
        return json.dumps({'valid': False}, ensure_ascii=False)

    conn = sqlite3.connect(database_name)
    c = conn.cursor()
    user_exists = len(c.execute('SELECT 1 FROM users WHERE token=? LIMIT 1;', (token,)).fetchall()) > 0
    if user_exists:
        c.execute('UPDATE users SET grade=? WHERE token=?', (grade, token,))
    else:
        c.execute('INSERT INTO users (token, grade, notification, daytime) VALUES (?, ?, ?, ?)',
                  (token, grade, False, '0700',))

        th_user_registered = str(c.execute('SELECT count(*) FROM users;').fetchone()[0]) + 'th user has registered.'
        print(th_user_registered)
        notifications.send_push_message(my_token, th_user_registered, 0, {'date': '99.99.9999', 'infos': [
            'Why do you click on that information notification?!'], 'lessons': []}, conn, c)
    conn.commit()
    conn.close()
    return json.dumps({'valid': True}, ensure_ascii=False)


@app.route('/notification_datas', methods=['POST'])
def get_notification_datas():
    data = request.get_json()
    password = decrypt_pw(data['password']) if 'password' in data else ''
    permission = data['permission']
    token = data['token']

    pw_valid = pw == password
    if not pw_valid or permission is None or str(permission) not in ("True", "False") or not token:
        return json.dumps({'valid': False}, ensure_ascii=False)

    if not permission or str(token) == '-':
        return json.dumps({'valid': True, 'value': False, 'daytime': '0700'}, ensure_ascii=False)

    conn = sqlite3.connect(database_name)
    c = conn.cursor()
    user_exists = len(c.execute('SELECT 1 FROM users WHERE token=? LIMIT 1;', (token,)).fetchall()) > 0
    if not user_exists:
        conn.close()
        return json.dumps({'valid': False}, ensure_ascii=False)
    c.execute('SELECT * FROM users WHERE token=?', (token,))
    row = c.fetchone()
    value = row[2] == 1
    daytime = row[3]
    conn.close()
    return json.dumps({'valid': True, 'value': value, 'daytime': daytime}, ensure_ascii=False)


@app.route('/notification', methods=['POST'])
def change_notification():
    data = request.get_json()
    password = decrypt_pw(data['password']) if 'password' in data else ''
    token = data['token']
    grade = data['grade']
    value = data['value']

    pw_valid = pw == password
    if not pw_valid or not token or value is None or str(value) not in ("True", "False"):
        return json.dumps({'valid': False}, ensure_ascii=False)

    conn = sqlite3.connect(database_name)
    c = conn.cursor()
    user_exists = len(c.execute('SELECT 1 FROM users WHERE token=? LIMIT 1;', (token,)).fetchall()) > 0
    if user_exists:
        c.execute('UPDATE users SET notification=? WHERE token=?', (value, token,))
    else:
        c.execute('INSERT INTO users (token, grade, notification, daytime) VALUES (?, ?, ?, ?)',
                  (token, grade, value, '0700',))
    conn.commit()
    conn.close()
    return json.dumps({'valid': True}, ensure_ascii=False)


@app.route('/daytime', methods=['POST'])
def change_daytime():
    data = request.get_json()
    password = decrypt_pw(data['password']) if 'password' in data else ''
    token = data['token']
    daytime = data['daytime']

    pw_valid = pw == password
    if not pw_valid or not token or not daytime or not re.match('^(([0-1][0-9])|(2[0-3]))[0-5][0-9]$', daytime):
        return json.dumps({'valid': False}, ensure_ascii=False)

    conn = sqlite3.connect(database_name)
    c = conn.cursor()
    user_exists = len(c.execute('SELECT 1 FROM users WHERE token=? LIMIT 1;', (token,)).fetchall()) > 0
    if not user_exists:
        conn.close()
        return json.dumps({'valid': False}, ensure_ascii=False)
    c.execute('UPDATE users SET daytime=? WHERE token=?', (daytime, token,))
    conn.commit()
    conn.close()
    return json.dumps({'valid': True}, ensure_ascii=False)


if __name__ == '__main__':
    serve(app, host="0.0.0.0", port=160)
