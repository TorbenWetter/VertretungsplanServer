import threading
import requests
import re
from datetime import datetime
from bs4 import BeautifulSoup, Comment


def get_date(plan_crawler):
    mon_title = plan_crawler['crawler'].find('div', class_='mon_title')
    if not mon_title:
        return None
    date_s = mon_title.text.strip().split(' ')[0]
    date = datetime.strptime(date_s, "%d.%m.%Y").date()
    return date


def format_date(date):
    day, week_day = date.split(' ')
    date_s = datetime.strptime(day, "%d.%m.%Y").date().strftime('%d.%m.%Y')
    return week_day[:2] + '., ' + date_s


def log(message):
    log_file = open('log.txt', 'a')
    log_file.write(message + '\n')
    log_file.close()


def decipher_grade(grade):
    grades = []
    grade_levels = re.compile('[0-9]+[a-zA-Z]*')  # e.g. ['7abc', '8d', '10'] for grade='7abc8d | 10'
    for grade_level in grade_levels.findall(grade):
        grade_parts = re.compile('[0-9]+|[a-zA-Z]+')  # e.g. ['7', 'abc'] for grade_level='7abc', ['10'] for '10'
        parts = grade_parts.findall(grade_level)
        if len(parts) == 1:  # e.g. ['10']
            grades.append(parts[0])  # e.g. '10'
        elif len(parts) == 2:  # e.g. ['7', 'abc']
            for letter in parts[1]:  # e.g. 'a', 'b', 'c'
                grades.append(parts[0] + letter.lower())  # e.g. '7a'
    return grades


def contains_grade(grade, subject, user_grade, user_courses):
    dec_grade = decipher_grade(grade)
    grade_number = int(re.compile('[0-9]+').findall(user_grade)[0])
    if 5 <= grade_number <= 10:
        return user_grade.lower() in dec_grade
    elif 11 <= grade_number <= 13 and user_courses:
        courses = user_courses.split('$')
        return user_grade in dec_grade and subject in courses
    return False


def prep_if_not_empty(prep, info):
    return (prep + ' ' + info) if info else ''


def decipher_lesson(lesson_parts):
    grade, lesson, substitution, teacher, subject, room, information, subst_of = lesson_parts

    lesson = re.compile('\s?-\s?').sub('. - ', lesson) + '. Stunde'
    room = 'R{}'.format(room) if re.compile('^[0-9]+').search(room) else room

    if substitution == '+':
        category = "eigenv. Arbeiten"
        description = prep_if_not_empty('in', subject) + ' ' + prep_if_not_empty('ohne', teacher)
        color = "#AEF7CF"
    elif not teacher and substitution:
        category = "Unterricht"
        description = prep_if_not_empty('in', subject) + ' ' + prep_if_not_empty('bei', substitution)
        color = "#FFFF77"
    elif substitution != teacher:
        category = "Vertretung"
        description = prep_if_not_empty('in', subject) + ' ' + prep_if_not_empty('bei', substitution)
        color = "#CD5C5C"
    else:
        if room and subject:
            category = "Raumänderung"
            description = prep_if_not_empty('in', subject) + ' ' + prep_if_not_empty('zu', room)
            room = ''  # so room isn't in both: description and room string
            color = "#9CC2E5"
        elif not subject:
            category = "Veranstaltung"
            description = prep_if_not_empty('in', room) + ' ' + prep_if_not_empty('bei', substitution)
            room = ''  # so room isn't in both: description and room string
            color = "#FF69B4"
        elif not room:
            category = "Entfall"
            description = prep_if_not_empty('von', subject) + ' ' + prep_if_not_empty('bei', teacher)
            color = "#92D050"
        else:
            category = "Fehler!"
            description = "Bitte mit Klasse/Kursen und Screenshot per Mail (torben@wetter.codes) " \
                          "oder auf Twitter/Instagram/YouTube/Facebook (Torben Wetter) melden!"
            color = "#FFFFFF"

    return [
        lesson,
        category,
        description.strip(),
        'in ' + room if room else room,
        information,
        'Vertr. von {}'.format(subst_of) if subst_of else '',
        color
    ]


def only_information(lesson_parts, info_index):
    for i, lesson_part in enumerate(lesson_parts):
        if i == info_index and not lesson_part or i != info_index and lesson_part:  # if info is empty or others aren't
            return False
    return True


def get_lessons(crawler, user_grade, user_courses):
    lessons = []
    lesson_lines = crawler.find('table', class_='mon_list').findAll('tr', {'class': ['list even', 'list odd']})
    for i in range(len(lesson_lines) - 1, -1, -1):  # list backwards loop
        lesson_line = lesson_lines[i]
        lesson_parts = [lesson_part.text for lesson_part in lesson_line.findAll('td', class_='list')]

        grade_index, subject_index, info_index = 0, 4, 6
        if only_information(lesson_parts, info_index):
            lesson_lines[i - 1].findAll('td', class_='list')[info_index].append(' ' + lesson_parts[info_index])
            del lesson_lines[i]
            continue

        if contains_grade(lesson_parts[grade_index], lesson_parts[subject_index], user_grade, user_courses):
            lesson = decipher_lesson(lesson_parts)
            lessons.append(lesson)
    return lessons[::-1]  # reverse again to normal direction


def get_infos(crawler):
    infos = []
    info_lines = crawler.find('table', class_='info').findAll('tr', class_='info')
    for info_line in info_lines:
        infos_in_line = info_line.findAll('td', class_='info')
        info_columns = len(infos_in_line)
        if info_columns == 1:
            infos_rows = infos_in_line[0].text.split('</br>')
            for infos_row in infos_rows:
                infos.append(infos_row)
        elif info_columns == 2:
            info_type = infos_in_line[0].text
            if info_type in ['Betroffene Klassen', 'Abwesende Klassen']:
                continue
            infos.append(info_type + ': ' + infos_in_line[1].text)
        elif info_columns > 2:
            log('Got an info with {} columns. lines: \n{}'.format(info_columns,
                                                                  '\n'.join([l.text for l in info_lines])))
    return infos


class Background:
    def __init__(self, pw_sha1):
        self.pw_sha1 = pw_sha1

        self.plans = {}
        self.request_plan_crawlers()

    def request_plan_crawlers(self):
        threading.Timer(20.0, self.request_plan_crawlers).start()

        gg_url = 'http://gutenberg-gymnasium.de'

        url = gg_url + '/vplanSchueler.php?passwort=' + self.pw_sha1
        content = requests.get(url).text

        crawler = BeautifulSoup(content, 'html.parser')

        path_start_s = 'Datei:.'
        comments = crawler.findAll(text=lambda text: isinstance(text, Comment))
        for comment in comments:
            comment = comment.strip()
            if not comment.startswith(path_start_s):
                continue
            path_start_index = comment.find(path_start_s) + len(path_start_s)

            path = comment[path_start_index:]
            url = gg_url + path
            content_decoded = requests.get(url).text

            # replace <b>, </b>, <u>, </u>, <i>, </i>, <strike>, </strike>, --- and &nbsp;
            content = re.compile('(</?(?i)(b|u|i|strike)>)|(---)|(&nbsp;)').sub('', content_decoded)

            crawler = BeautifulSoup(content, 'html.parser')

            self.plans[path] = {}
            self.plans[path]['crawler'] = crawler
            self.plans[path]['infos'] = get_infos(crawler)

    def get_plan_crawlers(self):
        crawlers = []
        for path in self.plans:
            if not get_date(self.plans[path]):
                continue
            crawlers.append(self.plans[path])
        crawlers.sort(key=get_date)
        return crawlers
